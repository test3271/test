from django.db import models

class Shelf(models.Model):
	name = models.CharField(max_length=200)

	def __str__(self):
		return self.name

class Book(models.Model):
	shelf = models.ForeignKey(Shelf, on_delete=models.CASCADE)
	title = models.CharField(max_length=200)
	author = models.CharField(max_length=100)
	description = models.CharField(max_length=200)
	status = models.BooleanField(default=True, choices=[
		(True, 'Available'),
		(False, 'Not available'),
	])

	def __str__(self):
		return self.title