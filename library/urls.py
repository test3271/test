from django.urls import path

from . import views

app_name = 'library'
urlpatterns = [
	path('', views.index, name='index'),
	path('<int:shelf_id>/', views.show_books, name='shelf'),
	path('<int:shelf_id>/<int:book_id>/', views.book, name='book'),
	path('<int:shelf_id>/add/', views.add_book, name='add_book'),
	path('<int:shelf_id>/<int:book_id>/edit/', views.edit_book, name='edit_book'),
	path('<int:shelf_id>/<int:book_id>/delete/', views.delete_book, name='delete_book'),
]