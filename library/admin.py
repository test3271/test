from django.contrib import admin

from .models import Book, Shelf

admin.site.register(Shelf)
admin.site.register(Book)
