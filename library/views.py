from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse

from .models import Shelf, Book

def index(request):
	context = {
		'shelves' : Shelf.objects.all(),
	}
	return render(request, 'library/index.html', context)

def show_books(request, shelf_id):
	shelf = get_object_or_404(Shelf, pk=shelf_id)
	all_books = shelf.book_set.all()
	context = {
		'shelf' : shelf,
		'books' : all_books,
	}
	return render(request, 'library/shelf.html', context)

def add_book(request, shelf_id):
	book = Book(
		shelf=Shelf.objects.filter(pk=shelf_id)[0],
		title=request.POST['title'],
		author=request.POST['author'],
		description=request.POST['description'],
		status=request.POST['status'],
	)
	book.save()
	return HttpResponseRedirect(reverse('library:shelf', args=(shelf_id,)))

def edit_book(request, shelf_id, book_id):
	book = get_object_or_404(Book, pk=book_id)
	book.title = request.POST['title']
	book.author = request.POST['author']
	book.description = request.POST['description']
	book.status = request.POST['status']
	book.save()
	return HttpResponseRedirect(reverse('library:book', args=(shelf_id,book_id)))
	
def delete_book(request, shelf_id, book_id):
	book = get_object_or_404(Book, pk=book_id)
	book.delete()
	return HttpResponseRedirect(reverse('library:shelf', args=(shelf_id,)))

def book(request, shelf_id, book_id):
	book = get_object_or_404(Book, pk=book_id)
	context = {
		'book': book,
		'shelf_id': shelf_id,
	}
	return render(request, 'library/book.html', context)

def take_book(request, book_id):
	book = get_object_or_404(Book, pk=book_id)
	if(book.status):
		book.status = False
		book.save()
		context = {'book': book, 'shelf_id': shelf_id}
	else:
		context = {
			'book': book,
			'shelf_id': shelf_id,
			'answer': "Sorry this book is currently not available."
		}
	return render(request, 'library/book.html', context)

def return_book(request, book_id):
	book = get_object_or_404(Book, pk=book_id)
	if(not book.status):
		book.status = True
		book.save()
	context = {'book': book, 'shelf_id': shelf_id}
	return render(request, 'library/book.html', context)
